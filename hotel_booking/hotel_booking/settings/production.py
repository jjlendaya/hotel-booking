from .base import *

ALLOWED_HOSTS = []

# Keep the secret key separate from the settings!
SECRET_KEY = env('DJANGO_SECRET_KEY')

# We set DEBUG mode to always be false in production
DEBUG = False
