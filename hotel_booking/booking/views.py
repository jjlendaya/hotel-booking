from django.utils import timezone
from django.shortcuts import get_object_or_404

from rest_framework import generics, status
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.response import Response
from rest_framework_filters.backends import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.serializers import ValidationError

from .models import Room, Hotel, RoomCheckInDate, RoomReservation, RoomBooking
from . import serializers
from .filters import RoomFilter
from .tasks import delete_user_reservation


# Create your views here.
class HotelViewSet(ReadOnlyModelViewSet):
    queryset = Hotel.objects.all()
    serializer_class = serializers.HotelSerializer
    permission_classes = (IsAuthenticated, )


class RoomList(generics.ListAPIView):
    serializer_class = serializers.RoomSerializer
    filter_backends = (DjangoFilterBackend, )
    filter_class = RoomFilter
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        queryset = Room.objects.all()
        query_params = self.kwargs['query_params']
        check_in_date = query_params.get('check_in_date', None)
        if check_in_date:
            queryset = queryset.filter(roomcheckindate__check_in_date=check_in_date)
        else:
            check_in_date__gte = query_params.get('check_in_date__gte', None)
            check_in_date__lt = query_params.get('check_in_date__lt', None)
            if check_in_date__gte and check_in_date__lt:
                queryset = queryset.filter(roomcheckindate__check_in_date__lt=check_in_date__lt,
                                           roomcheckindate__check_in_date__gte=check_in_date__gte)
            else:
                if check_in_date__gte:
                    queryset = queryset.filter(roomcheckindate__check_in_date__gte=check_in_date__gte)
    
                if check_in_date__lt:
                    queryset = queryset.filter(roomcheckindate__check_in_date__lt=check_in_date__lt)
        return queryset

    def get(self, request, *args, **kwargs):
        serializer = serializers.RoomQueryParamsSerializer(data=request.query_params)
        if serializer.is_valid():
            self.kwargs['query_params'] = serializer.validated_data
            return self.list(request, *args, **kwargs)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RoomDetail(generics.RetrieveAPIView):
    queryset = Room.objects.all()
    serializer_class = serializers.RoomSerializer
    permission_classes = (IsAuthenticated, )


class RoomCheckInDateList(generics.ListAPIView):
    queryset = RoomCheckInDate.objects.all()
    serializer_class = serializers.RoomCheckInDateSerializer
    permission_classes = (IsAuthenticated, )


class RoomReservationCreate(generics.CreateAPIView):
    queryset = RoomReservation.objects.all()
    serializer_class = serializers.RoomReservationSerializer
    permission_classes = (IsAuthenticated, )

    def perform_create(self, serializer):
        """
        We validate:
        1) whether the room exists,
        2) whether the room is available on the requested date,
        3) whether the room has already been reserved,
        4) and whether the room has already been booked.
        """
        room = get_object_or_404(Room, pk=self.kwargs['pk'])
        check_in_dates = room.roomcheckindate_set.filter(check_in_date=serializer.validated_data['reservation_date'])
        if check_in_dates.count() < 1:
            raise ValidationError('Room is not available on requested date')
        reservation_dates = room.roomreservation_set.filter(reservation_date=serializer.validated_data['reservation_date'])
        if reservation_dates.count() > 0:
            raise ValidationError('Room has already been reserved on that date')
        booking_dates = room.roombooking_set.filter(booking_date=serializer.validated_data['reservation_date'])
        if booking_dates.count() > 0:
            raise ValidationError('Room has already been booked on that date')
        else:
            serializer.save(
                room=room,
                user=self.request.user,
                created_at=timezone.now()
            )
            delete_user_reservation.delay(self.request.user.id, serializer.validated_data['reservation_date'])


class RoomBookingList(generics.ListAPIView):
    serializer_class = serializers.RoomBookingSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        display = self.kwargs['query_params'].get('display', None)
        queryset = RoomBooking.objects.all()
        if display:
            if display == 'past':
                queryset = queryset.filter(booking_date__lt=timezone.now().date())
            else:
                queryset = queryset.filter(booking_date__gt=timezone.now().date())
        return queryset

    def get(self, request, *args, **kwargs):
        serializer = serializers.RoomBookingQueryParamsSerializer(data=request.query_params)
        if serializer.is_valid():
            self.kwargs['query_params'] = serializer.validated_data
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return super(RoomBookingList, self).list(request, *args, **kwargs)


class RoomBookingCreate(generics.CreateAPIView):
    queryset = RoomBooking.objects.all()
    serializer_class = serializers.RoomBookingSerializer
    permission_classes = (IsAuthenticated, )

    def perform_create(self, serializer):
        """
        Here we validate the following:
        1) Room is not yet booked
        2) Reservation is present for the current user
        """
        room = get_object_or_404(Room, pk=self.kwargs['pk'])
        bookings = room.roombooking_set.filter(
            booking_date=serializer.validated_data['booking_date']
        )
        if bookings.count() > 0:
            raise ValidationError('Room has already been booked on that date')
        reservation = room.roomreservation_set.filter(
            user=self.request.user,
            reservation_date=serializer.validated_data['booking_date']
        )
        if reservation.count() > 0:
            serializer.save(
                room=room,
                user=self.request.user,
                created_at=timezone.now()
            )
        else:
            raise ValidationError('Room must be reserved before booking')
