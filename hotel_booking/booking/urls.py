from django.conf.urls import url

from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register(r'hotel', views.HotelViewSet, base_name='hotel')

app_name = 'booking'
urlpatterns = [
    url(r'^room/$', views.RoomList.as_view(), name='room-list'),
    url(r'^room/(?P<pk>[0-9]+)/$', views.RoomDetail.as_view(), name='room-detail'),
    url(r'^room/(?P<pk>[0-9]+)/check-in-date/$', views.RoomCheckInDateList.as_view(), name='room-check-in-date-list'),
    url(r'^room/(?P<pk>[0-9]+)/reserve/$', views.RoomReservationCreate.as_view(), name='room-reservation-create'),
    url(r'^room/(?P<pk>[0-9]+)/book/$', views.RoomBookingCreate.as_view(), name='room-booking-create'),
    url(r'^booking/$', views.RoomBookingList.as_view(), name='booking-list')
] + router.urls
