import rest_framework_filters as filters

from .models import Room, RoomCheckInDate, Hotel


class RoomCheckInDateFilter(filters.FilterSet):
    class Meta:
        model = RoomCheckInDate
        fields = {
            'check_in_date': ['exact', 'range']
        }


class HotelFilter(filters.FilterSet):
    class Meta:
        model = Hotel
        fields = {
            'name': ['exact', 'contains']
        }


class RoomFilter(filters.FilterSet):
    hotel = filters.RelatedFilter(HotelFilter,
                                  name='hotel',
                                  queryset=Hotel.objects.all())

    class Meta:
        model = Room
        fields = {
            'rate': ['exact', 'gt', 'lt'],
            'number_of_adults': ['exact'],
            'type': ['exact'],
            'facility_aircon': ['exact'],
            'facility_wifi': ['exact'],
            'facility_bath_type': ['exact']
        }
