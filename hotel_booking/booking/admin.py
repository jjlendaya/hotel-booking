from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Room, Hotel, RoomCheckInDate, User


# Register your models here.
admin.site.register(Room)
admin.site.register(Hotel)
admin.site.register(RoomCheckInDate)
admin.site.register(User, UserAdmin)
