from django.utils import timezone
from django.contrib.auth import get_user_model


class UserRequestLogMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if not request.user.is_anonymous:
            user = get_user_model().objects.get(pk=request.user.id)
            user.last_request_time = timezone.now()
            user.save()
        return response
