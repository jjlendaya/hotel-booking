from django.db import models
from django.contrib.auth.models import AbstractUser

from model_utils import Choices


# Create your models here.
class User(AbstractUser):
    last_request_time = models.DateTimeField(null=True, blank=True)


class Hotel(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Room(models.Model):
    ROOM_TYPES = Choices(
        ('single', 'Single'),
        ('double', 'Double'),
        ('twin', 'Twin'),
        ('queen', 'Queen'),
        ('dorm', 'Dorm')
    )
    BATH_TYPES = Choices(
        ('private', 'Private Bath'),
        ('shared', 'Shared Bath')
    )

    rate = models.DecimalField(max_digits=15, decimal_places=2)
    number_of_adults = models.IntegerField()
    type = models.CharField(max_length=10, choices=ROOM_TYPES)
    facility_aircon = models.BooleanField(default=False)
    facility_wifi = models.BooleanField(default=False)
    facility_bath_type = models.CharField(max_length=20, choices=BATH_TYPES)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    bookings = models.ManyToManyField(User, through='RoomBooking', related_name='room_booking_set')
    reservations = models.ManyToManyField(User, through='RoomReservation', related_name='room_reservation_set')

    def __str__(self):
        return '{} - {}: {}'.format(self.hotel.name, self.id, self.type)


class RoomCheckInDate(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    check_in_date = models.DateField()

    class Meta:
        unique_together = ('room', 'check_in_date')

    def __str__(self):
        return '{} - {}'.format(str(self.room), self.check_in_date.strftime('%Y-%m-%d'))


class RoomBooking(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    booking_date = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)


class RoomReservation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    reservation_date = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
