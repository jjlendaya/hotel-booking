from rest_framework import serializers

from .models import Hotel, Room, RoomCheckInDate, RoomReservation, RoomBooking


class HotelSerializer(serializers.ModelSerializer):
    rooms = serializers.PrimaryKeyRelatedField(many=True, read_only=True, source='room_set')

    class Meta:
        model = Hotel
        fields = ('id', 'name', 'rooms')


class RoomCheckInDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomCheckInDate
        fields = ('check_in_date', )


class RoomQueryParamsSerializer(serializers.Serializer):
    check_in_date = serializers.DateField(required=False)
    check_in_date__gte = serializers.DateField(required=False)
    check_in_date__lt = serializers.DateField(required=False)

    def validate(self, data):
        check_in_date = data.get('check_in_date', None)
        check_in_date__gte = data.get('check_in_date__gte', None)
        check_in_date__lt = data.get('check_in_date__lt', None)

        if check_in_date and check_in_date__lt and check_in_date__gte:
            raise serializers.ValidationError('You cannot pass all three date filters')
        elif (check_in_date and check_in_date__gte) or (check_in_date and check_in_date__lt):
            raise serializers.ValidationError('Must pass either only check_in_date or one of the range params')

        return data


class RoomSerializer(serializers.ModelSerializer):
    hotel = serializers.PrimaryKeyRelatedField(read_only=True)
    check_in_dates = RoomCheckInDateSerializer(many=True, read_only=True, source='roomcheckindate_set')

    class Meta:
        model = Room
        fields = ('id', 'hotel', 'rate', 'number_of_adults',
                  'type', 'facility_aircon', 'facility_wifi',
                  'facility_bath_type', 'check_in_dates')


class RoomBookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomBooking
        fields = ('user', 'room', 'booking_date', 'created_at')
        read_only_fields = ('user', 'room', 'created_at')


class RoomBookingQueryParamsSerializer(serializers.Serializer):
    display = serializers.ChoiceField(choices=('past', 'future'), required=False)


class RoomReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomReservation
        fields = ('user', 'room', 'created_at', 'reservation_date')
        read_only_fields = ('user', 'created_at', 'room')
