import time
import datetime

from django.utils import timezone
from celery import shared_task

from .models import RoomReservation, RoomBooking, User


@shared_task
def delete_user_reservation(user_id, reservation_date):
    reservation_date = datetime.datetime.strptime(reservation_date, '%Y-%m-%dT%H:%M:%S').date()
    while True:
        bookings = RoomBooking.objects.filter(user_id=user_id, booking_date=reservation_date)
        reservation = RoomReservation.objects.get(user_id=user_id, reservation_date=reservation_date)
        user = User.objects.get(pk=user_id)
        if timezone.now() > user.last_request_time + datetime.timedelta(minutes=5):
            if bookings.count() < 1:
                reservation.delete()
                return True
            else:
                return False
        else:
            time.sleep(20)
